import numpy as np

import assortative_network as an
import simulate_sample as ss

class ResearchAssortativity:
    def __init__(self, network_size, reps, batches, function, args):
        if reps % batches != 0:
            print("reps and batchess are not compatible")
         
        self.network_size = network_size if network_size <= 10000 else 10000
        self.reps = reps    
        self.batches = batches
        self.sampling_function = function
        self.args = args

    def assortivity_bias(self, evaluate_reps, evaluate_batches, network_size = None):
        """
        returns a list of tuples, where the first entry in every tuple 
        is the assortativity and the second entry the bias  
        """
        if not network_size:
            network_size = self.network_size
        
        assortativity_bias = []
        for assortativity in np.linspace(0.1, 0.9, 19, endpoint=True):
            print("the assortativity is now: {0}".format(assortativity))
            for i in range(self.reps):
                print("the rep is now: {0}".format(i))
                
                #change the below line to do it for a different type of network
                network_gen = an.AssortativeNetwork(
                    proportion_feature = 0.5, edges_per_new_node = 70, 
                    assortativity=assortativity, network_size = self.network_size
                    )
                
                network = network_gen.get_network()   
                network_assort = network_gen.get_assortativity()
                    
                simulation = ss.SimulateSample(network, evaluate_reps, evaluate_batches)
                self.set_samples(simulation)
                bias = simulation.get_bias() 
                assortativity_bias.append((network_assort, bias))
                
        return assortativity_bias       

    def assortivity_mean_abs_error(self, evaluate_reps, evaluate_batches, network_size = None):
        """
        returns a list of tuples, where the first entry in every tuple 
        is the assortativity and the second entry the mean_error  
        """
        if not network_size:
            network_size = self.network_size
        
        assortativity_mean_error = []
        for assortativity in np.linspace(0.05, 0.95, 19, endpoint=True):
            print("the assortativity is now: {0}".format(assortativity))
            for i in range(self.reps):
                print("the rep is now: {0}".format(i))
                
                #change the below line to do it for a different type of network
                network_gen = an.AssortativeNetwork(
                    proportion_feature = 0.5, edges_per_new_node = 70, 
                    assortativity=assortativity, network_size = self.network_size
                    )
                
                network = network_gen.get_network()   
                network_assort = network_gen.get_assortativity()
                    
                simulation = ss.SimulateSample(network, evaluate_reps, evaluate_batches)
                self.set_samples(simulation)
                errors = simulation.get_mean_error()
                errors = [item[0] for item in errors]
                mean_error = np.mean(errors)  
                assortativity_mean_error.append((network_assort, mean_error))
                
        return assortativity_mean_error       
                
    def set_samples(self, simulation):
        """given a simulation_sample object set samples using the samling_function
         string in the class """
        if self.sampling_function == "snowball": 
                    simulation.set_samples(simulation.snowball, self.args)
        elif self.sampling_function == "rds":
            simulation.set_samples(simulation.rds, self.args)
        elif self.sampling_function == "all_neighbors": 
            simulation.set_samples(simulation.all_neighbors, self.args)
        elif self.sampling_function == "all_neighbors2": 
            simulation.set_samples(simulation.all_neighbors2, self.args)
        elif self.sampling_function == "rds2": 
            simulation.set_samples(simulation.rds2, self.args)
        else:
            print("please give a valid function")
            
                    
                    
