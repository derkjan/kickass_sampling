import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np

import csv

assort     = []
mean_error = []

all_neighbors = 'monte_carlo_results/all_neighbors2.log'
rds_sampling  = 'monte_carlo_results/rds2+net10000_samples200_deg2_sampled200.log'
snowball_sampling = 'monte_carlo_results/snowball_net10000_samples200_deg2_sampled200.log'

title = "Snowball Sampling"

with open(snowball_sampling, 'rb') as csvfile:
	spamreader = csv.reader(csvfile, delimiter=';')
	for row in spamreader:
		assort.append(float(row[0]))
		mean_error.append(float(row[1]))

mean_error_series = pd.Series(np.array(mean_error))
assor_series = pd.Series(np.array(assort))

sns.regplot("assortativity", "mean abs error",
			pd.DataFrame({"assortativity": assor_series, "mean abs error": mean_error_series}))

sns.plt.title(title)
sns.plt.ylim(-0.2,0.6)
sns.plt.show()

sns.regplot("assortativity", "mean abs error",
			pd.DataFrame({"assortativity": assor_series, "mean abs error": mean_error_series}),
			order = 2,
			truncate = False)

sns.plt.title(title)
sns.plt.ylim(0.0,0.6)
sns.plt.show()






