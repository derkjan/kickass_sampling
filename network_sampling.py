import data_handling
import network_data

import numpy as np
import networkx as nx
import pandas as pd
import random

np.set_printoptions(precision=3, suppress = True)

def get_all_neighbors(network, max_depth):
    mean_features = []
    size_sample = []

    #Get random start node.
    neighbor_nodes = random.sample(network.nodes(), 1)
    size_sample.append(len(neighbor_nodes))

    #Set results array with features of start node.
    # results = [network.node[neighbor_nodes[0]]['features'][1]]
    results = [network.node[neighbor_nodes[0]].values()]

    #Get neighbors of start node and drop duplicates
    neighbors = nx.neighbors(network, neighbor_nodes[0])
    neighbors = np.unique(neighbors)

    size_sample.append(len(neighbors))

    mean_features.append((np.mean(results, axis=0)).tolist())                           # depth = 0

    for node in neighbors:
        # add_results = [network.node[node]['features'][1]]
        add_results = [network.node[node].values()]
        results = np.concatenate([results, add_results], axis = 0)

    mean_features.append((np.mean(results, axis=0)).tolist())                           # depth = 1

    i = 2
    while i < max_depth+1:
        i+=1

        for neighbor in neighbors:
            neighbors = np.concatenate([neighbors, nx.neighbors(network, neighbor)])

        neighbors = np.unique(neighbors)

        size_sample.append(len(neighbors))

        for node in neighbors:
            # add_results = [network.node[node]['features'][1]]
            add_results = [network.node[node].values()]
            results = np.concatenate([results, add_results], axis=0)

        mean_features.append((np.mean(results, axis=0)).tolist())                      # depth = 2 ... max_depth

    return size_sample, mean_features

def snowball_sampling(network, sample_size, number_of_neighbors):
    nodes = nx.nodes(network)
    sample = np.random.choice(nodes,1)

    i = 0

    while len(sample) < sample_size:
        if i == len(sample):
            return calculate_mean_var(results)

        neighbors    = nx.neighbors(network, sample[i])
        neighbors_of = random.sample(neighbors, min(number_of_neighbors, len(neighbors)))
        sample = np.concatenate([sample, neighbors_of], axis=0)

        sample = np.unique(sample)



        i+=1

        results = [network.node[sample[0]].values()]

        for node in sample[1:]:
            add_results = [network.node[node].values()]
            results = np.concatenate([results, add_results], axis=0)

    return calculate_mean_var(results)

def rds_sampling(network,start_points, number_of_neighbors, threshold):
    sample = []

    nodes = nx.nodes(network)
    start_nodes = np.random.choice(nodes, start_points)

    for node in start_nodes:
        neighbors    = nx.neighbors(network, node)
        neighbors_of = random.sample(neighbors, min(number_of_neighbors, len(neighbors)))

        for neighbor in neighbors_of:
            if random.random() > threshold:
                sample.append(neighbor)

    print len(sample)

    sample = np.unique(sample)

    print len(sample)

    results = [network.node[sample[0]].values()]

    for node in sample[1:]:
        add_results = [network.node[node].values()]
        results = np.concatenate([results, add_results], axis=0)

    return calculate_mean_var(results)


def calculate_mean_var(results):
    population_means = np.mean(results, axis=0)
    population_stds = np.std(results, axis=0)

    result_lst = []
    for i in range(len(population_means)):
        # result_lst.append((population_means[i], population_stds[i]** 2))
        result_lst.append(population_means[i])
    return result_lst

def population_mean(network):
    nodes = network.nodes()

    results = [network.node[nodes[0]].values()]

    for node in nodes[1:]:
        add_results = [network.node[node].values()]
        results = np.concatenate([results, add_results], axis=0)

    population_means = np.mean(results, axis=0)
    population_stds  = np.std(results, axis=0)

    result_lst = []
    for i in range(len(population_means)):
        result_lst.append((population_means[i],population_stds[i]**2))

    return result_lst

