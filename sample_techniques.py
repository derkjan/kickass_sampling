import numpy as np
import networkx as nx
import random

class Sample(object):
    def __init__(self, network):
        self.network = network

    def all_neighbors(self, max_depth, return_all_depth, return_network_graph, max_number_of_samples=1000000):
        '''
        Function to find all neighbors of neighbors to
        a certain max_depth.
        :param max_depth:
        :param return_all_depth: Boolean value whether to every depth result
        :return node list, nested list with feature average per depth:
        '''

        features_vector = []
        size_sample     = []

        # Get random start node.
        neighbor_nodes = random.sample(self.network.nodes(), 1)
        size_sample.append(len(neighbor_nodes))

        # Set results array with features of start node.
        results = [list(self.network.node[neighbor_nodes[0]].values())]

        # Get neighbors of start node and drop duplicates
        neighbors = nx.neighbors(self.network, neighbor_nodes[0])
        neighbors = np.unique(neighbors)
        size_sample.append(len(neighbors))
        features_vector.append((np.mean(results, axis=0)).tolist())  # depth = 0

        for node in neighbors:
            add_results = [list(self.network.node[node].values())]
            results = np.concatenate([results, add_results], axis=0)
        features_vector.append((np.mean(results, axis=0)).tolist())  # depth = 1

        i = 2
        while i < max_depth + 1:
            i += 1

            for neighbor in neighbors:
                neighbors = np.concatenate([neighbors, nx.neighbors(self.network, neighbor)])

            neighbors = np.unique(neighbors)

            size_sample.append(len(neighbors))

            for node in neighbors:
                add_results = [list(self.network.node[node].values())]
                results = np.concatenate([results, add_results], axis=0)

            features_vector.append((np.mean(results, axis=0)).tolist())  # depth = 2 ... max_depth

        if(return_network_graph):
            network_nodes = self.network.nodes()
            remove = [val for val in network_nodes if val not in neighbors]
            G = self.network.copy()
            G.remove_nodes_from(remove)
            return G
        elif return_all_depth:
            return neighbors, features_vector
        else:
            return neighbors, features_vector[len(features_vector)-1]

    def all_neighbors2(self, sample_size, return_network_graph=True):
        '''
        Function to find all neighbors of neighbors to
        a certain max_depth.
        :param max_depth:
        :param return_all_depth: Boolean value whether to every depth result
        :return node list, nested list with feature average per depth:
        '''
        
        nodes = nx.nodes(self.network)
        sample = np.random.choice(nodes, 1)

        i = 0
        while len(sample) < sample_size and i < len(sample):
            neighbors = nx.neighbors(self.network, sample[i])
            neighbors = [item for item in neighbors if item not in sample]
            sample = np.concatenate([sample, neighbors], axis=0)
                    
            i += 1
        
        if len(sample) > 100:
            sample = sample[:100]
             
        if(return_network_graph):
            network_nodes = self.network.nodes()
            remove = [val for val in network_nodes if val not in sample]
            G = self.network.copy()
            G.remove_nodes_from(remove)
            return G
        else:
            return sample


    def snowball(self, sample_size, number_of_neighbors, return_network_graph):
        '''
        Function to find all neighbors using snowball sampling
        to a certain depth by selecting number_of_neighbors
        neighbors to select.
        :param sample_size: max number of samples
        :param number_of_neighbors: number of neighbors to select per neighbor
        :return: node list
        '''

        nodes = nx.nodes(self.network)
        sample = np.random.choice(nodes, 1)

        i = 0
        while len(sample) < sample_size and i < len(sample):
            neighbors = nx.neighbors(self.network, sample[i])
            neighbors = [item for item in neighbors if item not in sample]
            neighbors_of = random.sample(neighbors, min(number_of_neighbors, len(neighbors)))
            sample = np.concatenate([sample, neighbors_of], axis=0)
                    
            i += 1

        if(return_network_graph):
            network_nodes = self.network.nodes()
            remove = [val for val in network_nodes if val not in sample]
            G = self.network.copy()
            G.remove_nodes_from(remove)
            return G
        else:
            return sample

    def rds(self, number_of_startpoints, number_of_neighbors, sample_size, threshold, return_network_graph):
        '''
        first pick max 3 neighbors, then test them to see whether you accept them 
        Respondent Driven Sampling
        :param number_of_startpoints: number of points to start sampling from
        :param number_of_neighbors: max number of neighbors per node to sample
        :param threshold: neighbor acceptance probability
        :return: sample
        '''
        sample = []

        nodes = nx.nodes(self.network)
        start_nodes = np.random.choice(nodes, number_of_startpoints)

        for node in start_nodes:
            neighbors = nx.neighbors(self.network, node)
            neighbors_of = random.sample(neighbors, min(number_of_neighbors, len(neighbors)))

            for neighbor in neighbors_of:
                if random.random() > threshold:
                    sample.append(neighbor)

        sample = list(np.unique(sample))

        i = 0
        while len(sample) < sample_size and i < len(sample):
            neighbors = nx.neighbors(self.network, sample[i])
            neighbors = [item for item in neighbors if item not in sample]
            neighbors_of = random.sample(neighbors, min(number_of_neighbors, len(neighbors)))

            for neighbor in neighbors_of:
                if random.random() > threshold:
                   sample.append(neighbor)
            
            i += 1

        #results = [list(self.network.node[sample[0]].values())]
        #for node in sample[1:]:
        #    add_results = [list(self.network.node[node].values())]
        #    results = np.concatenate([results, add_results], axis=0)
        # return self.__mean_results(results)

        if(return_network_graph):
            network_nodes = self.network.nodes()
            remove = [val for val in network_nodes if val not in sample]
            G = self.network.copy()
            G.remove_nodes_from(remove)
            return G
        else:
            return sample

    def rds2(self, number_of_startpoints, number_of_neighbors, sample_size, threshold, return_network_graph=True):
        '''
        first pick max 3 neighbors, then test them to see whether you accept them 
        Respondent Driven Sampling
        :param number_of_startpoints: number of points to start sampling from
        :param number_of_neighbors: max number of neighbors per node to sample
        :param threshold: neighbor acceptance probability
        :return: sample
        '''

        nodes = nx.nodes(self.network)
        start_nodes = np.random.choice(nodes, number_of_startpoints)
        sample = list(start_nodes)
        
        i = 0
        while len(sample) < sample_size and i < len(sample):
            neighbors = nx.neighbors(self.network, sample[i])
            
            #delete duplicates
            neighbors = [item for item in neighbors if item not in sample]
            
            #test whether to select a node
            neighbors = [item for item in neighbors if random.random() > threshold]
            
            #sample at max three neighbours
            neighbors_of = random.sample(neighbors, min(number_of_neighbors, len(neighbors)))
            sample += neighbors_of
            
            i += 1

        if(return_network_graph):
            network_nodes = self.network.nodes()
            remove = [val for val in network_nodes if val not in sample]
            G = self.network.copy()
            G.remove_nodes_from(remove)
            return G
        else:
            return sample

    def rds_nr(self, number_of_neighbors, sample_size, threshold, return_network_graph):
        '''
        Non-Random Respondent Driven Sampling with start nodes far from central
        :param number_of_startpoints: number of points to start sampling from
        :param number_of_neighbors: number of neighbors per startpoint to sample
        :param threshold: neighbor acceptance probability
        :return: list with nodes.
        '''
        sample = []

        start_node, start_nodes = self.__rds_nodes()
        sample.append(start_node)

        for node in start_nodes:
            neighbors = nx.neighbors(self.network, node)
            neighbors_of = random.sample(neighbors, min(number_of_neighbors, len(neighbors)))

            for neighbor in neighbors_of:
                if random.random() > threshold:
                    sample.append(neighbor)

        sample = list(np.unique(sample))

        i = 0
        while len(sample) < sample_size and i < len(sample):
            if len(sample) < sample_size:
                neighbors = nx.neighbors(self.network, sample[i])
                neighbors_of = random.sample(neighbors, min(number_of_neighbors, len(neighbors)))

                for neighbor in neighbors_of:
                    if random.random() > threshold:
                        sample.append(neighbor)

                sample = list(np.unique(sample))

            i += 1

        results = [list(self.network.node[sample[0]].values())]

        for node in sample[1:]:
            add_results = [list(self.network.node[node].values())]
            results = np.concatenate([results, add_results], axis=0)

        # return self.__mean_results(results)

        if(return_network_graph):
            network_nodes = self.network.nodes()
            remove = [val for val in network_nodes if val not in sample]
            G = self.network.copy()
            G.remove_nodes_from(remove)
            return G
        else:
            return sample

    def calculate_mean_features(self, sample):
        '''
        Calculate average feature value (per feature) for given nodes in sample
        :param sample: sample (e.g. Snowball, rds, rds_nr
        :return: list with average feature value per feature.
        '''
        results = [list(self.network.node[sample[0]].values())]

        for node in sample[1:]:
            add_results = [list(self.network.node[node].values())]
            results = np.concatenate([results, add_results], axis=0)
        
        return np.mean(results, axis = 0)

    def calculate_mean_error_features(self, sample):
        '''
        Calculate mean error of feature values (per feature) for given nodes in sample
        :param sample: sample (e.g. Snowball, rds, rds_nr
        :return: list with mean error feature value per feature.
        '''
        results = [list(self.network.node[sample[0]].values())]

        for node in sample[1:]:
            add_results = [list(self.network.node[node].values())]
            results = np.concatenate([results, add_results], axis = 0)

        return self.__mean_error(results)

    def get_population_mean(self):
        '''
        calculate population feature value mean per feature
        :return: list with average feature value per feature plus sd
        '''
        nodes = self.network.nodes()

        results = [list(self.network.node[nodes[0]].values())]

        for node in nodes[1:]:
            add_results = [list(self.network.node[node].values())]
            results = np.concatenate([results, add_results], axis=0)
            
        population_means = np.mean(results, axis=0)
        population_stds = np.std(results, axis=0)

        result_lst = []
        for i in range(len(population_means)):
            result_lst.append((population_means[i], population_stds[i] ** 2))

        return result_lst

    def __mean_error(self, results):
        '''
        Function returns a list with mean error (w.r.t. population means)
        :param results: list with sample means (w.r.t. features)
        :return: list with mean errors for features
        '''
        sample_mean = np.mean(results, axis = 0)
        population_mean  = self.get_population_mean()

        mu_lst = []
        for i in population_mean:
            mu_lst.append(i[0])
        mu_lst = np.array(mu_lst)

        result = np.abs(sample_mean - mu_lst)

        return result

    def __network_distances(self, center_node):
        network_nodes = self.network.nodes()
        maximum = 0

        distances = dict()

        for node in network_nodes:
            distance = len(nx.shortest_path(self.network, center_node, node)) - 1

            if distance > maximum:
                maximum = distance

            distances[node] = distance

        node_lst = []
        node_lst_max = []
        for key, value in distances.iteritems():
            if value == maximum or value == maximum - 1:
                node_lst.append(key)

            if value == maximum:
                node_lst_max.append(key)

        return node_lst, node_lst_max

    def __node_distance(self, start_node, nodes):
        maximum = 0
        distances = dict()

        for node in nodes:
            distance = len(nx.shortest_path(self.network, start_node, node)) - 1

            if distance > maximum:
                maximum = distance

            distances[node] = distance

        node_lst = []

        for key, value in distances.iteritems():
            if value == maximum or value == maximum - 1 or value == maximum - 2:
                node_lst.append(key)

        return node_lst

    def __rds_nodes(self):
        start_node = random.sample(nx.center(self.network),1)[0]
        selected_nodes, selected_max =  self.__network_distances(start_node)

        first_node = random.sample(selected_max, 1)[0]

        second_node_list = self.__node_distance(first_node, selected_nodes)
        second_node = random.sample(second_node_list, 1)[0]

        # THIRD NODE:
        third_node_list = self.__node_distance(second_node, selected_nodes)

        b3 = [val for val in second_node_list if val in third_node_list]

        distances = dict()
        minimum = 100
        for node in b3:
            distance1 = len(nx.shortest_path(self.network, first_node, node)) - 1
            distance2 = len(nx.shortest_path(self.network, second_node, node)) - 1

            distance_measure = min(distance1, distance2)
            distances[node] = distance_measure

            if distance_measure < minimum:
                minimum = distance_measure

        node_lst = []
        for key, value in distances.iteritems():
            if value == minimum:
                node_lst.append((key))

        try:
            third_node = random.sample(node_lst, 1)[0]
        except:
            print("Exception: sample larger than population")
            return start_node, [first_node, second_node]

        fourth_node_list = self.__node_distance(third_node, selected_nodes)

        b4 = [val for val in b3 if val in fourth_node_list]

        distances = dict()
        minimum = 100
        for node in b4:
            distance1 = len(nx.shortest_path(self.network, first_node, node)) - 1
            distance2 = len(nx.shortest_path(self.network, second_node, node)) - 1
            distance3 = len(nx.shortest_path(self.network, third_node, node)) - 1

            distance_measure = min(distance1, min(distance2, distance3))
            distances[node] = distance_measure

            if distance_measure < minimum:
                minimum = distance_measure

        node_lst = []
        for key, value in distances.iteritems():
            if value == minimum:
                node_lst.append((key))

        try:
            fourth_node = random.sample(node_lst, 1)[0]
        except:
            print("Exception: sample larger than population")
            return start_node, [first_node, second_node, third_node]


        return start_node, [first_node, second_node, third_node, fourth_node]

