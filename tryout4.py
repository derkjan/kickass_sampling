import pandas as pd
import numpy as np
import sample_techniques as st
import build_assortative as ba
import assortative_network as an
import networkx as nx
import simulate_sample as ss

"""
def add_features(network, features):

    params:
        network: a networkx class
        features: a dataframe where the row index resembles the nodes of
        the network
    

    for i in features.index.values:
        if i in network.nodes():
            for j in features.columns.values:
                network.node[i][str(j)] = features.loc[i, j]
    return network


network = an.AssortativeNetwork(0.5, 5, 0.5, 10).get_network()
print(network.number_of_edges())

features = pd.DataFrame(np.array([[1,2],[3,4]]))
print(features)

add_features(network, features)

print(network.node[0])
"""

"""
sample = st.Sample(network.get_network())


print(sample.snowball(100, 3, True).number_of_nodes())
print(sample.all_neighbors2(100, True).number_of_nodes())

print(sample.rds2(number_of_startpoints = 5, number_of_neighbors=3, sample_size=100, threshold=0.5).number_of_nodes())
"""

"""
build = ba.AssortativeScaleFree(0.5, 2, 0.5)
build.add_nodes(500)
print(build.G.number_of_edges())
"""

def add_number_to_file(number):
    with open('rds2_gplus.log', mode='a', encoding='utf-8') as a_file:
        a_file.write(str(number))
        a_file.write(";")

reps = 300

G = nx.read_gpickle("big_google_plus_file")
print(len(G.nodes()))
#G = G.to_undirected()

#G = max(nx.connected_component_subgraphs(G), key=len)


print(len(G.nodes()))

print(G.node[G.nodes()[2]])
simulation = ss.SimulateSample(G, 1, 1)
#print(simulation.snowball(150, 5, True).number_of_nodes())
#print(simulation.rds2(5, 3, 150, 0.4, True).number_of_nodes())

mean_errors = []

for i in range(reps):
    simulation.set_samples(simulation.rds2, (5, 5, 150, 0.3, True))
    #simulation.set_samples(simulation.snowball, (150, 5, True))
    print(simulation.samples[0].number_of_nodes())
    if simulation.samples[0].number_of_nodes()>30:
        print("in the if") 
        errors = simulation.get_mean_error()
        errors = [item[0] for item in errors]
        mean_error = np.mean(errors)  
        add_number_to_file(mean_error)
        mean_errors.append(mean_error)

print(mean_errors)         

#print(simulation.snowball(100, 5, True).number_of_nodes())
#print(simulation.rds2(5, 3, 200, 0.4, True).number_of_nodes())


