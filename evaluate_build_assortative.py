import networkx as nx
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import network_statistics as net_stats
import build_assortative as gen
from sklearn import linear_model

class EvaluateBuildAssortative:
    def __init__(self, network_size=1000, new_node_edges=2, reps=5, prop_feature=0.5)
        self.network_size = self.network_size
        self.new_node_edges = self.new_node_edges
        self.reps = reps
        self.prop_feature = prop_feature

    ba_power_law_stats = []


    #the classic ba algorithm
    def stats_barabasi_albert():
        for j in range(self.reps):
            ba_g = nx.barabasi_albert_graph(self.network_size, self.new_node_edges)
            ba_power_law_stats.append(net_stats.fit_powerlaw(ba_g))

        ba_mean = np.mean([stats["coef"] for stats in ba_power_law_stats])    
        ba_mean_stdev = np.std([stats["coef"] for stats in ba_power_law_stats])
        ba_stdev = np.mean([stats["std"] for stats in ba_power_law_stats])     
        print("the ba mean is: {0}, the standard deviation of the mean {1}, the mean of the standard dev is: {2}".format(ba_mean, ba_mean_stdev, ba_stdev))

    def stats_build_assortative():
        examined_values = np.linspace(0.1, 0.9, num=9, endpoint = True)
        power_law_evaluation_stats = dict()
        assortativity_values = []

        #the ba algorithm that allows for tuning the assortativity of one feature 
        for i in examined_values: 
            power_law_evaluation_stats[i] = []
            assortativity_stats = []
            for j in range(self.reps):
                a = gen.AssortativeScaleFree(proportion_feature = self.prop_feature, edges_per_new_node = self.new_node_edges, weight_similarity = i)
                a.add_nodes(self.network_size)
                power_law_evaluation_stats[i].append(net_stats.fit_powerlaw(a.G))
                assortativity_stats.append(nx.attribute_assortativity_coefficient(a.G, "feat"))
        
            assortativity_values.append(assortativity_stats)
        
        #net_stats.get_degree_distribution_plot(a.G)
        #print("the assortativity constant is: {}".format(nx.attribute_assortativity_coefficient(a.G, "feat")))
        
        assor_means = [np.mean(row) for row in assortativity_values]
        assor_stdev = [np.std(row) for row in assortativity_values]
        
        
        clf = linear_model.LinearRegression()
        clf.fit (np.array([[y-0.5] for y in examined_values]), assor_means)
        print(examined_values)
        print(assor_means)
        print("the coefficient equals: {0}".format(clf.coef_))
        
        coef_mean = []
        coef_mean_stdev = []
        coef_stdev = []
        
        for i in examined_values:
            coef_mean.append(np.mean([stats["coef"] for stats in power_law_evaluation_stats[i]]))
            coef_mean_stdev.append(np.std([stats["coef"] for stats in power_law_evaluation_stats[i]]))    
            coef_stdev.append(np.mean([stats["std"] for stats in power_law_evaluation_stats[i]]))     

        plt.figure(1)
        plt.ylabel('connection probability')
        plt.ylabel('assortativity')
        plt.plot(examined_values, assor_means, 'r')
        plt.plot(examined_values, assor_stdev, 'g')
        plt.show()

        plt.figure(2)
        plt.title('the coefficent values and their standard dev and the mean of the standard dev')
        plt.plot(examined_values, coef_mean, 'r')
        plt.plot(examined_values, coef_mean_stdev, 'b')
        plt.plot(examined_values, coef_stdev, 'g')
        plt.show()

