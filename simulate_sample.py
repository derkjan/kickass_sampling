import numpy as np
import networkx as nx
import matplotlib.pylab as plt
from scipy.stats import linregress

import assortative_network as an
import sample_techniques as st

class SimulateSample(st.Sample):
    def __init__(self, network, reps, batches):
        super(SimulateSample, self).__init__(network)
        self.reps = reps
        self.batches = batches
        self.samples = None #self.set_samples(self, function, args, reps)
        
    def get_frequency_nodes(self):
        samples = [node for sample in self.samples for node in sample.nodes()]    
        return {node: samples.count(node) for node in samples}
        
    def set_samples(self, function, args, reps=None):
        if not reps:
            reps=self.reps
        self.samples = [function(*args) for i in list(range(reps))]
    
    def get_batched_means(self, number_list, batches=None):
        if batches is None:
            batches = self.batches
        
        batched_list = []
        count = 0
        while count+batches <= len(number_list):
            batched_list.append(np.mean(number_list[count:count+batches]))
            count += batches
            
        return batched_list       
    
    def get_diameter_dist(self):
        return [nx.diameter(network) for network in self.samples]

    def __mean_std_run_reps(self):
        """
        returns:
            feature_vector: a matrix containing on the rows for every
            sample size the mean for every attribute (on the columns)
            std_values_lower: same as feature vector but then for lower 
            bounds
            std_values_upper: same as feature vector but then for upper 
            bounds   
        """
        a = self.calculate_mean_error_features(self.samples[0].nodes())
        feature_vector = np.copy(a)
        std_values_upper  = feature_vector 
        std_values_lower  = feature_vector 
        
        for iter in range(1, len(self.samples), 1):
            b = self.calculate_mean_error_features(self.samples[iter].nodes())
            a = np.vstack((a, b))

            std_lst  = np.std(a, axis=0)
            mean_lst = np.mean(a, axis=0)

            feature_vector = np.vstack((feature_vector, mean_lst))

            upper = (mean_lst + (1.96 * std_lst / np.sqrt(iter + 2)))
            lower = (mean_lst - (1.96 * std_lst / np.sqrt(iter + 2)))

            std_values_upper = np.vstack((std_values_upper, upper))
            std_values_lower = np.vstack((std_values_lower, lower))

        return feature_vector, std_values_lower, std_values_upper

    def get_bias(self):
        """returns an array with the absolute bias per feature"""
        pop_mean = self.get_population_mean()[0][0]
        average_over_samples = self.__mean_std_run_reps()[0][-1:][0][0]
        return np.absolute(pop_mean - average_over_samples)
        
    def get_mean_error(self):
        pop_mean = self.get_population_mean()
        pop_mean = [item[0] for item in pop_mean]
        
        error = []
        for iter in range(len(self.samples)):
            mean_feat_sample = self.calculate_mean_features(self.samples[iter].nodes())
            error.append(np.absolute(mean_feat_sample - pop_mean))
        
        return error
                
    def get_features(self):    
        """return a list containing all the features of the network"""
        return list(self.network.nodes(data=True)[0][1].keys())

    def plot2(self):
        mean_values, std_values_lower, std_values_upper = self.__mean_std_run_reps()
        Number_of_trials = np.arange(len(self.samples))

        # PLOTTING PART
        for j in range(len(mean_values[0])-1):  # jth column
            print(j, len(mean_values[0]))

            fig, ax = plt.subplots()

            # set left axis
            ax.plot(Number_of_trials,
                    mean_values[:, j],
                    'r',
                    label = 'sample mean error')

            ax.set_ylabel('Mean Error', color = 'k')

            plt.fill_between(Number_of_trials,
                             std_values_lower[:, j],
                             std_values_upper[:, j],
                             color = 'r',
                             alpha = 0.2)

            for tl in ax.get_yticklabels():
                tl.set_color('k')

            plt.title('Mean Error Plot, feature: %s' % (j + 1))  # count in index numbers!
            plt.show()

    def plot(self):
        mean_values, std_values_lower, std_values_upper = self.__mean_std_run_reps()
        pop_mean = self.get_population_mean()
        Number_of_trials = np.arange(len(self.samples))
        N = len(self.samples)

        # CREATE PLOTTING LISTS/ARRAYS
        mu_lst = []
        for i in pop_mean:
            mu_lst.append(i[0])
        mu_lst = np.array(mu_lst)

        error_matrix = np.zeros((N, len(mu_lst)))
        for i in range(len(mean_values)):
            error_matrix[i, :] = abs(mean_values[i, :] - mu_lst)

        # PLOTTING PART
        for j in range(len(mu_lst)):  # jth column

            fig, ax1 = plt.subplots()

            # set left axis
            ax1.plot(Number_of_trials, mean_values[:, j], 'r', label='sample mean')
            ax1.set_ylabel('Mean convergence', color='k')
            plt.fill_between(Number_of_trials, std_values_lower[:, j], std_values_upper[:, j], color='r',
                             alpha=0.2)
            for tl in ax1.get_yticklabels():
                tl.set_color('k')

            # add mu value line on the left axis scale
            plt.axhline(y=mu_lst[j], xmin=0, xmax= len(mu_lst), label='population mean')
            plt.legend(loc=2)

            # set right axis
            ax2 = ax1.twinx()
            ax2.plot(Number_of_trials, error_matrix[:, j], 'k', label='absolute error', alpha = 0.8)
            ax2.set_ylabel('Absolute Error', color='k')
            for tl in ax2.get_yticklabels():
                tl.set_color('k')
            plt.legend(loc=1)

            plt.title('error & convergence plot, feature: %s' % (j + 1))  # count in index numbers!
            plt.show()

    def assortativity(self):
        mean_values, std_values_lower, std_values_upper = self.__mean_std_run_reps()
        pop_mean = self.get_population_mean()
        N = len(self.samples)

        # CREATE PLOTTING LISTS/ARRAYS
        mu_lst = []
        for i in pop_mean:
            mu_lst.append(i[0])
        mu_lst = np.array(mu_lst)

        error_matrix = np.zeros((N, len(mu_lst)))
        for i in range(len(mean_values)):
            error_matrix[i, :] = abs(mean_values[i, :] - mu_lst)

        H = self.samples[0]

        feature_lst = H.node[H.nodes()[0]].keys()
        assortativity = []
        for assor in feature_lst:
            assortativity.append(nx.attribute_assortativity_coefficient(H, assor))

        Y_error = []
        for k in range(len(assortativity)):
            Y_error.append(error_matrix[N-1, k])

        slope, intercept, r_value, p_value, std_err = linregress(assortativity, Y_error)

        print ("Slope: %s \nintercept %s \nr_value %s \np_value %s \nstd_err %s") % (slope, intercept, r_value, p_value, std_err)
        
        
