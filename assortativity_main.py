import research_assortativity as ra
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np

def main():
    #research = ra.ResearchAssortativity(network_size=7000, reps=3, batches=1, function="all_neighbors2", args=(100, True))
    #print("all neighbour sampling")
    research = ra.ResearchAssortativity(network_size=7000, reps=3, batches=1, function="snowball", args=(200, 3, True))
    print("snowball_sampling")
    #research = ra.ResearchAssortativity(network_size=7000, reps=3, batches=1, function="rds2", args=(5, 3, 200, 0.5, True))
    #print("rds2_sampling")
    
    assort_mean_error = research.assortivity_mean_abs_error(network_size=7000, evaluate_reps=75, evaluate_batches=1)        
    
    assort = [item[0] for item in assort_mean_error]
    mean_error = [item[1] for item in assort_mean_error]
    plt.plot(assort, mean_error, '.')
    plt.show()
    
    mean_error_series = pd.Series(np.array(mean_error))
    assor_series = pd.Series(np.array(assort))
    sns.regplot("assor", "mean_error", pd.DataFrame({"assor":assor_series, "mean_error":mean_error_series}))
    sns.plt.show()
    
    sns.regplot("assor", "mean_error", pd.DataFrame({"assor":assor_series, "mean_error":mean_error_series}), order = 2, truncate = False)
    sns.plt.show()
    
    with open('snowball_70.log', mode='w', encoding='utf-8') as a_file:
        for line in assort_mean_error:
            a_file.write(str(line[0]))
            a_file.write(";")
            a_file.write(str(line[1]))
            a_file.write("\n")
    
if __name__ == "__main__":
    main()
