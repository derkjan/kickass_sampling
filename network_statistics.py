import matplotlib.pyplot as plt
import networkx as nx
import powerlaw
import random

def fit_powerlaw(network):
    """returns a dict with the coefficients and its standard deviation"""
    evaluation = dict()
    
    degree = network.degree()
    fit = powerlaw.Fit(list(degree.values()))
    
    #get the parameter of the powerlaw that gives the best fit
    evaluation['coef'] = fit.power_law.alpha
    
    #get the standard deviation of the estimate
    evaluation['std'] = fit.power_law.sigma
    
    #compare to other distributions a higher R (first in tuple) is better and p (second in tuple)
    #indicates the probability of it being random standard 0.05 cutoff
    #evaluation['to_exp']=fit.distribution_compare('power_law', 'exponential', normalized_ratio=True)
    #evaluation['to_log_normal'] = fit.distribution_compare('power_law', 'lognormal', normalized_ratio=True)
    return evaluation



def get_undirected_graph(dataframe):
    return nx.from_pandas_dataframe(dataframe, 0, 1)

def get_directed_graph(dataframe):
    return nx.from_pandas_dataframe(dataframe, 0, 1, create_using = nx.DiGraph())

def get_number_of_edges(network):
    return network.size()

def get_number_of_nodes(network):
    return network.number_of_nodes()

def get_nodes(network):
    return network.nodes()

def get_average_degree(network):
    return float(get_number_of_edges(network)) / get_number_of_nodes(network)

def get_max_diameter(network):
    return nx.diameter(network)

def get_clustering_coefficient(network):
    return nx.clustering(network)

def get_average_clustering(network):
    return nx.average_clustering(network)

def get_closeness_centrality(network):
    return nx.closeness_centrality(network)

def get_betweenness_centrality(network):
    return nx.betweenness_centrality(network)

def get_degree_distribution_plot(Network):
    # calculate the degrees for all nodes in the network
    degree_lst = list(nx.degree_histogram(Network))

    # creates a log-log plot of the degree distrubution (power law plot)
    plt.figure()
    plt.loglog(degree_lst, 'b.')
    plt.xlabel('Degree')
    plt.ylabel('Frequency')
    plt.title('Degree Distribution')
    plt.show()

    # creates a histogram of the degree distribution
    plt.figure()
    plt.hist(degree_lst)
    plt.xlabel('Degree')
    plt.ylabel('Frequency')
    plt.title('Degree Distribution Histogram')
    plt.show()

def get_components(network):
    return sorted(nx.connected_component_subgraphs(network), key = len, reverse=True)

def get_largest_component(network):
    components = get_components(network)
    largest_comp = components[0]

    largest_comp_nodes = get_nodes(largest_comp)
    nodes = get_nodes(network)

    remove_nodes = [x for x in nodes if x not in largest_comp_nodes]

    network.remove_nodes_from(remove_nodes)

    return network

def plot_statistics(measure):
    # Measure input: betweenness_centrality, clustering_coefficient, closeness_centrality

    measure_lst = []
    for i in measure:
        measure_lst.append(measure[i])
    plt.plot(measure_lst, 'k.')
    plt.show()

def plot_degree_hist(network):
    deg_hist = nx.degree_histogram(network)

    plt.loglog(range(len(deg_hist)), deg_hist, 'b.')
    plt.xlabel('Degree')
    plt.ylabel('Frequency')
    plt.title('Degree Distribution')
    plt.show()

def get_center_nodes(network):
    center_nodes = nx.center(network)
    return random.sample(center_nodes,1)[0]

def get_network_distance(network, center_node):
    network_nodes = network.nodes()
    maximum = 0

    distances = dict()

    for node in network_nodes:
        distance = len(nx.shortest_path(network,center_node, node))-1

        if distance > maximum:
            maximum = distance

        distances[node] = distance

    node_lst = []
    node_lst_max = []
    for key, value in distances.iteritems():
        if value == maximum or value == maximum - 1:
            node_lst.append(key)

        if value == maximum:
            node_lst_max.append(key)

    return node_lst, node_lst_max

def get_node_distance(network, start_node, nodes):
    maximum = 0
    distances = dict()

    for node in nodes:
        distance = len(nx.shortest_path(network, start_node, node)) - 1

        if distance > maximum:
            maximum = distance

        distances[node] = distance

    node_lst = []

    for key, value in distances.iteritems():
        if value == maximum or value == maximum -1 or value == maximum -2:
            node_lst.append(key)

    return node_lst

def determine_rds_nodes(network, selected_nodes, selected_max):
    first_node  = random.sample(selected_max,1)[0]

    second_node_list = get_node_distance(network, first_node, selected_nodes)
    second_node = random.sample(second_node_list,1)[0]

    #THIRD NODE:
    third_node_list = get_node_distance(network, second_node, selected_nodes)

    b3 = [val for val in second_node_list if val in third_node_list]

    distances = dict()
    minimum = 100
    for node in b3:
        distance1 = len(nx.shortest_path(network, first_node , node)) - 1
        distance2 = len(nx.shortest_path(network, second_node, node)) - 1

        distance_measure = min(distance1,distance2)
        distances[node] = distance_measure

        if distance_measure < minimum:
            minimum = distance_measure

    node_lst = []
    for key, value in distances.iteritems():
        if value == minimum:
            node_lst.append((key))

    third_node =  random.sample(node_lst,1)[0]

    fourth_node_list = get_node_distance(network, third_node, selected_nodes)

    b4 = [val for val in b3 if val in fourth_node_list]

    distances = dict()
    minimum = 100
    for node in b4:
        distance1 = len(nx.shortest_path(network, first_node , node)) - 1
        distance2 = len(nx.shortest_path(network, second_node, node)) - 1
        distance3 = len(nx.shortest_path(network, third_node , node)) - 1

        distance_measure = min(distance1, min(distance2, distance3))
        distances[node] = distance_measure

        if distance_measure < minimum:
            minimum = distance_measure

    node_lst = []
    for key, value in distances.iteritems():
        if value == minimum:
            node_lst.append((key))

    fourth_node = random.sample(node_lst, 1)[0]

    return [first_node, second_node, third_node, fourth_node]
