import networkx as nx
import pandas as pd
import numpy as np

class AssortativeScaleFree(object):
    def __init__(self, proportion_feature, edges_per_new_node, weight_similarity):
        self.binary_list = []
        self.weight_assortativity = weight_similarity
        self.proportion_feature = proportion_feature
        self.G = nx.Graph()
        self.edges_per_new_node = edges_per_new_node
        self.initialise_graph(edges_per_new_node)
        
    def initialise_graph(self, edges_per_new_node):
        if edges_per_new_node == 2:
            self.add_node(0)
            self.add_node(1)
            self.G.add_edge(0, 1)
            self.neighbour_list = [1, 1]
        else:
            for i in range(edges_per_new_node):
                self.add_node(i)
            
            edges = []
            j = 1
            for i in range(edges_per_new_node):
                j = i + 1
                while j<edges_per_new_node:
                    edges.append((i, j))
                    j += 1
            
            self.G.add_edges_from(edges)
            self.neighbour_list = [edges_per_new_node-1 for item in range(edges_per_new_node)]
        
    def add_nodes(self, n, similarity_weight = None):
        if similarity_weight is None:
            similarity_weight = self.weight_assortativity
        start = max(self.G.nodes()) + 1
        for i in range(start, start+n): 
            self.add_node(i)
            
            if self.G.node[i]["feat"]==1: 
                bin_list = self.binary_list[:-1]
            else: 
                bin_list = [1 if i==0 else 0 for i in self.binary_list[:-1]]
            
            selected_nodes = self.select_edges(i, self.G.nodes()[:-1], self.normalize_list(self.adjust_assortativity(bin_list, similarity_weight)))
            
            #add_edges
            #binary_list = nx.get_node_attributes(self.G, "feat").values
            self.G.add_edges_from([(i, node) for node in selected_nodes])
                        
            #update_neighbour_list
            self.neighbour_list.append(self.edges_per_new_node)
            for node in selected_nodes:
                self.neighbour_list[node] += 1
            
    def add_node(self, number):    
        if np.random.rand() < self.proportion_feature:
            self.G.add_node(number, feat = 1)
            self.binary_list.append(1)
        else:
            self.G.add_node(number, feat = 0)
            self.binary_list.append(0)         
            
    def select_edges(self, node, nodes, weighted_list):  
        return np.random.choice(nodes, self.edges_per_new_node, replace = False, p = weighted_list)
        
    def normalize_list(self, first_list):
        sum_list = sum(first_list)
        return [item/sum_list for item in first_list]
        
    def adjust_assortativity(self, binary_list, p):  
        weighted_list = self.create_weighted_list(binary_list, p)
        return list(np.multiply(self.neighbour_list, weighted_list))
    
    def create_weighted_list(self, binary_list, p):
        return [p if i==1 else 1-p for i in binary_list]
               
