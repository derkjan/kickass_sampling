import networkx as nx
import numpy as np

class Theoretical_Sampling:
    
    def __init__(self, graph_size = 1000, number_of_nodes_ba = 5, chance_random_graph = 0.1, k_nearest_neighbours_ws = 5, rewiring = 0.1):
        self.ba_g = nx.barabasi_albert_graph(graph_size, number_of_nodes_ba)
        self.random_graph = fast_gnp_random_graph(graph_size, chance_random_graph)
        self.watts_strogatz_graph(graph_size, k_nearest_neighbours_ws, p, seed=None)[source]

