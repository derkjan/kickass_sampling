import numpy as np
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
import seaborn as sns

import assortative_network as an
import simulate_sample as ss

class EvaluateNetwork:
    def __init__():
        self.assortative = an.AssortativeNetwork(proportion_feature=0.5, edges_per_new_node=2, assortativity=0.7, network_size=2000)
        self.simulation = ss.SimulateSample(assortative.get_network(), reps = 100, batches = 10)
        print("ready network")

        simulation.set_samples(function=simulation.snowball, args=(100, 3, True), reps=100)
        print("simulation ready")

    def estimate_diameter_by_sampling(self):
        diam_list = self.simulation.get_diameter_dist()
        batch_means = self.simulation.get_batched_means(diam_list, 10)
        print_mean_std(batch_means)

    def print_diam(self, network):
        print("the diameter of the network is: {0}".format(nx.diameter(network)))    

    def print_assortativity(self, network, feat):
        print("the diameter of the network is: {0}".format(nx.attribute_assortativity_coefficient(network, feat)))

    def print_mean_std(self, sample_list):
        print("the batched mean is {0} and the batched std is: {1}".format(np.mean(sample_list), np.std(sample_list)))    

"""
plt.plot(list(simulation.get_frequency_nodes().values()))
plt.show()
sns.distplot(np.array(list(simulation.get_frequency_nodes().values())))
sns.plt.show()
print("simulation ready")
print(simulation.samples[0].nodes())
"""

