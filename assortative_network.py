import networkx as nx
import numpy as np
import build_assortative 


class AssortativeNetwork(build_assortative.AssortativeScaleFree):
    """
    small class that builds a scale-free network with a defined assortativity
    the class only works well for proportion_feature (which proportion of the
    population gets the attribute) values of 0.3 to 0.7
    """
    def __init__(self, proportion_feature, edges_per_new_node, assortativity, network_size):
        super(AssortativeNetwork, self).__init__(proportion_feature, edges_per_new_node, assortativity/2 + 0.5)
        self.add_nodes(network_size)  
    
    def get_network(self):
        return self.G
    
    def get_assortativity(self):    
        return nx.attribute_assortativity_coefficient(self.get_network(), "feat")    
