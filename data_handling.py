import pandas as pd
import numpy as np
import networkx as nx
import pickle
import os

class CreateGraphs:
    def __init__(self, network_data, number_of_features = 10): 
        self.number_of_features = number_of_features         
        self.folder = network_data['folder']  
        self.name = network_data['name']
        self.undirected = network_data['undirected']
        self.file_numbers = network_data['file_numbers']
        #self.networks = self.create_networks()

    #realise that the features are shifted by 1 compared to the ones in the file: .featnames 
    def create_networks(self):
        networks = dict()    
        for number in self.file_numbers:
            df_features = pd.read_csv(self.folder + number + '.feat', delim_whitespace = True, header = None, index_col = 0)
            df_selected_features = self.select_most_frequent_features(df_features)
               
            df_edges = pd.read_csv(self.folder + number + '.edges', delim_whitespace = True, header = None)
            if self.undirected:
                G = nx.from_pandas_dataframe(df_edges, 0, 1, edge_attr=None, create_using=None)
            else:
                G = nx.from_pandas_dataframe(df_edges, 0, 1, edge_attr=None, create_using=nx.DiGraph())    
            
            for i in G.nodes():
                for j in df_selected_features.columns.values:
                    G.node[i][str(j)] = df_selected_features.loc[i, j]
                    
            networks['G' + number] = G   
        
        return networks
    
    def add_features(network, features):
        """
        params:
            network: a networkx class
            features: a dataframe where the row index resembles the nodes of
            the network
        """

        for i in features.index.values:
            if i in network.nodes:
                for j in df_selected_features.columns.values:
                    network.node[i][str(j)] = df_selected_features.loc[i, j]
        
        return network
    
    # select most frequent features per column, it assumes binary data
    def select_most_frequent_features(self, dataframe):
        most_frequent_features = dataframe.sum(axis=0).sort_values(ascending=False).iloc[0:self.number_of_features].index.values
        return dataframe.loc[:, most_frequent_features.tolist()]
            
    def write_all_networks_to_folder(self):
        for number in self.file_numbers:        
            save_network_to_file(self.networks['G' + number], 'pickle_files/' + 'G' + number + '.gpickle')
        
    def save_network_to_file(self, network, file_path):
        nx.write_gpickle(network, file_path)
        
    def read_network(self, file_path = 'pickle_files/' + "G0.gpickle"):
        return nx.read_gpickle(file_path)




