import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from sklearn import linear_model
import numpy as np
import scipy as sc
from scipy.stats.stats import pearsonr
from scipy import stats
import sys

def find_correlation(file_name="test1.log"):
    df = pd.read_csv(file_name, sep = ";", header = None)
    X = df.iloc[:, 0:1]
    y = df.iloc[:, 1:2]

    clf = linear_model.LinearRegression()
    clf.fit (X.values, y.values)
    print("the coefficient equals: {0}".format(clf.coef_)[0][0])

    slope, intercept, r_value, p_value, std_err = stats.linregress(
        df.iloc[:, 0:1].values.flatten(), df.iloc[:, 1:2].values.flatten() 
        )
        
    message = ("the slope equals: {0}, the intercept equals: {1} the r value"+
               "is {2}, the p value is: {3}, the standard error is: {4}")
    print(message.format(slope, intercept, r_value, p_value, std_err))

if __name__ == "__main__":
    try:
        find_correlation(sys.argv[1])
    except:
        find_correlation()
