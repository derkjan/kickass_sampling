from network_sampling import *
from network_statistics import *

import seaborn as sns
import data_handling
import simulate_sample
import sample_techniques
import network_data
import numpy as np
import time

np.set_printoptions(precision=3,
                    suppress = True)

b = network_data.Network_Data()
facebook_data = b.facebook

a = data_handling.CreateGraphs(network_data = facebook_data,
                               number_of_features = 5)

network = a.create_networks()['G1912']
network = get_largest_component(network)

def use_classes(network):
    print("network created")
    start = time.time()

    simulation = simulate_sample.SimulateSample(network, 3)

    print("Simulation created in %s") % (time.time() - start)
    start = time.time()

    simulation.set_samples(simulation.snowball, [70, 3, True], 500)
    # simulation.set_samples(simulation.rds, [30, 5, 0.5, True], 500)

    print("Simulation done in %s") % (time.time() - start)
    start = time.time()

    simulation.plotting_simulation_results()

    print("Plots view for a total of %s seconds") % (time.time() - start)

def use_sample_class(network):
    sample = sample_techniques.Sample(network)

    nodes, features = sample.all_neighbors(max_depth = 2,
                                           return_all_depth = False,
                                           return_network_graph = False)
    print features
    print ("1 done \n")

    snowball = sample.snowball(sample_size = 70,
                               number_of_neighbors = 3,
                               return_network_graph = True)

    print len(snowball.nodes())
    print sample.calculate_mean_features(sample = snowball.nodes())
    print ("2 done \n")


    rds = sample.rds(number_of_startpoints = 30,
                     number_of_neighbors = 5,
                     threshold = 0.5,
                     return_network_graph = True)

    print sample.calculate_mean_features(sample = rds.nodes())
    print ("3 done \n")


    rds_nr = sample.rds_nr(number_of_neighbors = 5,
                           threshold = 0.5,
                           return_network_graph = True)


    print sample.calculate_mean_features(sample = rds_nr.nodes())
    print ("4 done \n")

use_classes(network)

# use_sample_class(network)