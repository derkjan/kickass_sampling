import numpy as np
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
import seaborn as sns

import assortative_network as an
import simulate_sample as ss

#df = pd.DataFrame(np.arange(12).reshape((3,4)))
#print(df)
#print(df.loc[1,2])
#print(df.sum(axis=0))

"""
plt.plot([1,2,3,4], [1, 4, 9, 16])
plt.plot([1,2,3,4], [1, 0.5, 0.3, 1.7])
plt.show()


a = {"coef": 1}
b = {"coef": 2}
c = {"coef": 3}

[a, b, c]

print([stats["coef"] for stats in [a, b, c]])
from sklearn import linear_model
clf = linear_model.LinearRegression()
clf.fit (np.array([[y] for y in [0, 2, 4]]), [0, 1, 2])
print(clf.coef_)
"""

"""
#some simple test written
a = an.AssortativeNetwork(proportion_feature=0.5, edges_per_new_node=2, assortativity=0.7, network_size=2000)
print(nx.attribute_assortativity_coefficient(a.get_network(), "feat"))
"""

a = an.AssortativeNetwork(proportion_feature=0.5, edges_per_new_node=2, assortativity=0.7, network_size=2000)
print("the diameter of the network is: {0}".format(nx.diameter(a.get_network())))

simulation = ss.SimulateSample(a.get_network(), reps = 100, batches = 10)
print("ready network")

simulation.set_samples(function=simulation.snowball, args=(100, 3, True), reps=100)
print("simulation ready")

diam_list = simulation.get_diameter_dist()
print(diam_list)
batch_means = simulation.get_batched_means(diam_list, 10)
print(batch_means)

print("the batched mean is {0} and the batched std is: {1}".format(np.mean(batch_means), np.std(batch_means)))

"""
plt.plot(list(simulation.get_frequency_nodes().values()))
plt.show()
sns.distplot(np.array(list(simulation.get_frequency_nodes().values())))
sns.plt.show()
print("simulation ready")
print(simulation.samples[0].nodes())
"""


