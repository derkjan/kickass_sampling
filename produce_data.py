import pandas as pd
import networkx as nx
import numpy as np

import data_handling as dh

def f(x): 
    if x[-5:] == ".feat":
        return True
    else:
        return False

def select_most_frequent_features(self, dataframe, number_of_features):
    most_frequent_features = dataframe.sum(axis=0).sort_values(ascending=False).iloc[0:number_of_features].index.values
    return dataframe.loc[:, most_frequent_features.tolist()]

def add_features(network, features):
    """
    params:
        network: a networkx class
        features: a dataframe where the row index resembles the nodes of
        the network
    """
    
    
    count = 0
    for i in features.index.values:
        if i in network.nodes():
            count += 1
            for j in features.columns.values:
                network.node[i][str(j)] = features.loc[i, j]
    
    print(count)
    return network
    
df = pd.read_csv("file_names_gplus.txt", sep =  ";", names= ["file_names"], header = None)
df_select = df.loc[df.loc[:, "file_names"].map(f), :] 
df_arr = df_select.values


df_edges = pd.read_csv("gplus_combined.txt", delim_whitespace=True, header=None)
print("read_data_file")
G = nx.from_pandas_dataframe(df_edges.iloc[0:df_edges.shape[0]//5, :], 0, 1, edge_attr=None, create_using=nx.DiGraph())

select_file_names = []
list_gender = []
for i in range(df_arr.shape[0]):
    df_temp = pd.read_csv("data_files/gplus/" + df_arr[i][0][:-5] + ".featnames", sep =  ";", names= ["file_names"], header = None)
    if not df_temp.iloc[0:3, :].empty and df_temp.iloc[0:3, :].iloc[0:1, 0:1].values[0][0] == "0 gender:1": 
        select_file_names.append("data_files/gplus/" + df_arr[i][0])

print("somewhat further")
print("hello")
print(len(select_file_names))
print("the length of the file names is {}".format(len(select_file_names)))

count = 0
for file_path in select_file_names:
    features = pd.read_csv(file_path, delim_whitespace = True, header = None, index_col = 0, usecols = [0, 1, 2])
    G = add_features(G, features)
    count += 1
    if count%10 == 0:
        print("the count is {}".format(count))
        print(G.node[G.nodes()[count//2]])

nx.write_gpickle(G, "big_google_plus_file")    
 
"""
for file_name in self.file_numbers:
    df_features = pd.read_csv(folder + file_name, delim_whitespace = True, header = None, index_col = 0)
    df_selected_features = self.select_most_frequent_features(df_features)
    add_features(G, df_selected_features)       
           
"""


